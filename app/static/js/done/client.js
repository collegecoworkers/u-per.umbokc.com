var selfEasyrtcid = "";

function disable(domId) {
	document.getElementById(domId).disabled = "disabled";
}

function enable(domId) {
	document.getElementById(domId).disabled = "";
}

function connect() {
	console.log("Initializing.");
	easyrtc.enableVideo(false);
	easyrtc.enableVideoReceive(false);
	easyrtc.initMediaSource(function(){        // success callback
		easyrtc.connect("easyrtc.audioOnly", loginSuccess, loginFailure);
	}, function(errorCode, errmesg){
		easyrtc.showError(errorCode, errmesg);
	});
}

function disconnect() {
	easyrtc.hangupAll();
	easyrtc.disconnect();
	console.log("disconnecting from server");
	enable("connectButton");
	disable("disconnectButton");
	document.getElementById('status').innerText = 'Звонок прерван';
}

function loginSuccess(easyrtcid) {
	selfEasyrtcid = easyrtcid;
	disable("connectButton");
	enable("disconnectButton");
	document.getElementById('status').innerText = 'Идет звонок...';
	document.getElementById('id_number').innerText = 'Ваш id ' + easyrtcid;
	console.log(easyrtcid);
}

function loginFailure(errorCode, message) {
	easyrtc.showError(errorCode, message);
}

easyrtc.setStreamAcceptor( function(easyrtcid, stream) {
	var audio = document.getElementById('callerAudio');
	easyrtc.setVideoObjectSrc(audio,stream);
	enable("disconnectButton");
	document.getElementById('status').innerText = 'Звонок начался, говорите';
	document.getElementById('id_number').innerText = '';
});

easyrtc.setOnStreamClosed( function (easyrtcid) {
	easyrtc.setVideoObjectSrc(document.getElementById('callerAudio'), "");
	disable("disconnectButton");
	document.getElementById('status').innerText = 'Звонок прерван';
});

easyrtc.setAcceptChecker(function(easyrtcid, callback) {
	if( easyrtc.getConnectionCount() > 0 ) {
	} else {
		callback(true);
		console.log('callback(true)');
	}
});
