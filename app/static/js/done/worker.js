U.dbg = (mes) => {
	console.log(mes);
	window.dbg_m = mes;
};

var selfEasyrtcid = "";

function disable(domId) {
	document.getElementById(domId).disabled = "disabled";
}

function enable(domId) {
	document.getElementById(domId).disabled = "";
}

function connect() {
	console.log("Initializing.");
	easyrtc.enableVideo(false);
	easyrtc.enableVideoReceive(false);
	easyrtc.setRoomOccupantListener(convertListToButtons);
	easyrtc.initMediaSource(function(){ // success callback
		easyrtc.connect("easyrtc.audioOnly", loginSuccess, loginFailure);
	}, function(errorCode, errmesg){
		easyrtc.showError(errorCode, errmesg);
	}); // failure callback
}

function terminatePage() {
	easyrtc.disconnect();
}

function hangup() {
	easyrtc.hangupAll();
	disable("hangupButton");
	U.each(U.$gets('.list-group-item button'), function(el) {
		el.disabled = '';
		U.text(el, 'Ответить');
	});
}

function clearConnectList() {
	otherClientDiv = document.getElementById('otherClients');
	while (otherClientDiv.hasChildNodes()) {
		otherClientDiv.removeChild(otherClientDiv.lastChild);
	}
}

function convertListToButtons (roomName, occupants, isPrimary) {
	U.dbg([roomName, occupants, isPrimary]);
	clearConnectList();
	var otherClientDiv = document.getElementById('otherClients');
	for(var easyrtcid in occupants) {
    var item = document.createElement('li');
    var button = document.createElement('button');

    item.classList.add('list-group-item');

    button.innerText = 'Ответить';

    button.classList.add('btn');
    button.classList.add('btn-lg');
    button.classList.add('btn-default');
    button.setAttribute('fl:r', '');

    button.onclick = function(easyrtcid) {
      return function(e) {
      	U.text(e.target, 'Звонок начался');
        performCall(easyrtcid);
      };
    }(easyrtcid);

    var label = document.createElement('text');
    label.innerHTML = easyrtc.idToName('Пользователь ' + easyrtcid);

    item.appendChild(label);
    item.appendChild(button);

    otherClientDiv.appendChild(item);
	}
}

function performCall(otherEasyrtcid) {
	easyrtc.hangupAll();
	var acceptedCB = function(accepted, caller) {
		if( !accepted ) {
			easyrtc.showError("CALL-REJECTED", "Sorry, your call to " + easyrtc.idToName(caller) + " was rejected");
			enable('otherClients');
		}
	};
	var successCB = function() {
		enable('hangupButton');
		U.each(U.$gets('.list-group-item button'), function(el) {
			el.disabled = 'disabled';
		});
	};
	var failureCB = function() {
		enable('otherClients');
	};
	easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
}

function loginSuccess(easyrtcid) {
	disable("connectButton");
	enable("disconnectButton");
	enable('otherClients');
	selfEasyrtcid = easyrtcid;
	U.dbg(easyrtcid);
}

function loginFailure(errorCode, message) {
	easyrtc.showError(errorCode, message);
}

function disconnect() {
	easyrtc.hangupAll();
	easyrtc.disconnect();
	console.log("disconnecting from server");
	disable("disconnectButton");
	enable("connectButton");
}

easyrtc.setStreamAcceptor( function(easyrtcid, stream) {
	var audio = document.getElementById('callerAudio');
	easyrtc.setVideoObjectSrc(audio,stream);
	console.log('no is calling');
	enable("hangupButton");
});

easyrtc.setOnStreamClosed( function (easyrtcid) {
	console.log('stop calling');
	easyrtc.setVideoObjectSrc(document.getElementById('callerAudio'), "");
	disable("hangupButton");
});

easyrtc.setAcceptChecker(function(easyrtcid, callback) {
	document.getElementById('acceptCallBox').style.display = "block";
	if( easyrtc.getConnectionCount() > 0 ) {
		document.getElementById('acceptCallLabel').textContent = "Drop current call and accept new from " + easyrtcid + " ?";
	}
	else {
		document.getElementById('acceptCallLabel').textContent = "Accept incoming call from " + easyrtcid + " ?";
	}
	var acceptTheCall = function(wasAccepted) {
		document.getElementById('acceptCallBox').style.display = "none";
		if( wasAccepted && easyrtc.getConnectionCount() > 0 ) {
			easyrtc.hangupAll();
		}
		callback(wasAccepted);
	};
	document.getElementById("callAcceptButton").onclick = function() {
		acceptTheCall(true);
	};
	document.getElementById("callRejectButton").onclick =function() {
		acceptTheCall(false);
	};
});
